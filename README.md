# Endpoints syntax

## Users

### GET requests :

*   `/users` : Return list of users with the specified filter in JSON
    *   *String* `searchQuery` : Return only user with this specified pseudo, all users if `''`
    *   *Number* `limit` : Limit number of elements on a page (give all elements if not used)
    *   *Number* `page` : Number of pages to return (exemple : if `page = 3` and `limit = 100`, response will contain elements `201` to `300`)
---
*   `/users/{id}` : Return the specific user in JSON
---
*   `/users/count` : Return the number of a users in JSON

### POST requets :

*   `/users` : Create a new user and return it in JSON
    *   *String* `email` : email of the new user
    *   *String* `name` : pseudo of the new user

### PUT requets :

*   `/users/{id}` : Modify this user and return it in JSON
    *   *String* `pseudo` : pseudo of the new user
    *   *String* `image` : image url for user's profile picture


## Posts

### GET requests :

*   `/users/{id}/posts` : Return list of a user's posts (from the newest to the oldest) in JSON
    *   *Number* `limit` : Limit number of elements on a page (give all elements if not used)
    *   *Number* `page` : Number of pages to return (exemple : if `page = 3` and `limit = 100`, response will contain elements `201` to `300`)
---
*   `/users/{id}/posts/count` : Return the number of a user's posts in JSON
---
*   `/users/{id}/followees/posts` : Return all posts (from the newest to the oldest) from all followees of the specified user in JSON (in this specific case, the post will contain its publisher)
    *   *Number* `limit` : Limit number of elements on a page (give all elements if not used)
    *   *Number* `page` : Number of the page to return (exemple : if `page = 3` and `limit = 100`, response will contain elements `201` to `300`)

### POST requests :

*   `/users/{id}/posts` : Create new post and return it in JSON
    *   *String* `image` : image url of new post
    *   *String* `description`: new post's description


## Followers / Followees

### GET requests :

*   `/users/{id}/followers` : Return a list of a user's followers (list of users in JSON)
    *   *Number* `limit` : Limit number of elements on a page (give all elements if not used)
    *   *Number* `page` : Number of pages to return (exemple : if `page = 3` and `limit = 100`, response will contain elements `201` to `300`)
---
*   `/users/{id}/followers/count` : Return the number of a user's followers in JSON
---
*   `/users/{id}/followees` : Return list of user's followees (list of users in JSON)
    *   *Number* `limit` : Limit number of elements on a page (give all elements if not used)
    *   *Number* `page` : Number of the page to return (exemple : if `page = 3` and `limit = 100`, response will contains elements `201` to `300`)
---
*   `/users/{id}/followees/count` : Return number of user's followees in JSON
---
*   `/users/{userId}/followees/{followeeId}` : Return the subscription of this user on this followee, no response if the user dindn't follow the followee.

### POST requests :

*   `/users/{id}/followees` : add a followee to my followee list (make a subscription to someone) and return the subscription in JSON
    *   *String* `followeeId` : id of the followee to add

### DELETE requests :

*   `/users/{userId}/followees/{followeeId}` : remove the followee to my followee list (unfollow) and return the subscription in JSON

## Likes

### GET requests :

*   `/posts/{id}/likes` : Return list of user who liked this post (list of users in JSON)
    *   *Number* `limit` : Limit number of elements on a page (give all elements if not used)
    *   *Number* `page` : Number of the page to return (exemple : if `page = 3` and `limit = 100`, response will contains elements `201` to `300`)
---
*   `/posts/{id}/likes/count` : Return likes number on this post in JSON
---
*   `/posts/{postId}/likes/{userId}` : Return the like of this user on this post, no response if the user didn't have like this post

### POST requests :

*   `/posts/{id}/likes` : add a like on this post and return it in JSON
    *   *String* `userId` : id of the user who like this post

### DELETE requests :

*   `/posts/{postId}/likes/{userId}` : remove a like and return it in JSON

## Tests

### POST requests :

*   `/tests` : create `n` users, all users will subscribe to all users before himself, all users will create posts (`1` to `n` respectively from last one to first one), and every users will like the last post of all his subscriptions. (result : `n` users, `n(n-1)/2` subscriptions, `n(n+1)/2` posts and `n(n-1)/2` likes)
    *   *Number* `number` : number of user to create

### DELETE requests :

*   `/tests` : clean up all database's objects. *WARNING !!! : your account will be removed to, remember to log out and log in before to test things on the application*


# JSON objects content

## User

#### User main object

```js
{
    "id" : String, // user id
    "email" : String, // user email
    "pseudo" : String, // user pseudo
    "profileImage" : String // image url of the user's profile picture
}
```

#### Users count

```js
{
    "usersCount" : Number
}
```

## Post

#### Post main object

```js
{
    "id" : String, // post id
    "userId" : String, // id of the user who create this post
    "image" : String, // image url of the post
    "description" : String, // post's description
    "date" : String, // date and time of the post according ISO 8601 full date time format
    //optional
    "user" : UserMainObject // Use only with the GET /users/{id}/followees/posts endpoint
}
```

#### Posts count

```js
{
    "postsCount" : Number
}
```

## Subscription

#### Subscription main object

```js
{
    "followerId" : String, // user id of the follower (user who follow)
    "followeeId" : String, // user id of the followee (user who is followed)
}
```

#### Followers count

```js
{
    "followersCount" : Number
}
```

#### Followees count

```js
{
    "followeesCount" : Number
}
```

## Like

#### Like main object

```js
{
    "userId" : String, // id of the user who like this post
    "postId" : String, // id of the post who is liked
}
```

#### Likes count

```js
{
    "likesCount" : Number
}
```

## List

```js
[
  JSONMainObject,
  JSONMainObject,
  JSONMainObject,
  ...
]
```

## Kinds

```yaml
- kind: Post
  ancestor: no
  properties:
  - name: userId
    direction: asc
  - name: date
    direction: desc
```
