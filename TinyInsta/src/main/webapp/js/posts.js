document.addEventListener('DOMContentLoaded', function() {
	var elems = document.querySelectorAll('.modal');
	var instances = M.Modal.init(elems);
});

document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);
});

var id = getCookie("userId");

var Posts = {
    view: function() {
        return m("div", {class:"row col s12"},  [
        		m("div", {class:"row col s6 offset-s3"}, [
        			
        			post.map(function(value, index)
	    	        {
        				return m("div", {class:"square waves-effect waves-light btn modal-trigger", onclick: async function(){await isLikedBy(value.userId, value.postId, `modal${index}Like`, `likeCount${index}`)}, href:"#modal"+index, style:"background-image: url('" + value.image + "');"}, [])
	    	        }),
	    	        post.map(function(value, index){
	    	        	return m("div", {id:"modal"+index, class:"modal"}, [
        					m("div", {class:"modal-content remove-padding"}, [
        						m("div", {class:"remove-margin remove-padding row"}, [
        							m("div", {class:"col s8 image-container left blue-grey lighten-4"}, [	
        								(() => {
        									if(value.imageWidth > value.imageHeight)
        	    	        				{
        	    	        					return m("img", {class:"remove-padding materialboxed thumbLandScape", style:"margin-left:auto;margin-right:auto;", src:value.image})	 
        	    	        				}
        	    	        				else
        	    	        				{
        	    	        					return m("img", {class:"remove-padding materialboxed thumbPortrait", style:"margin-left:auto;margin-right:auto;", src:value.image})   	        					
        	    	        				}
        								})()
	            					]),
	        						m("div", {class:"col s4 right row remove-padding remove-margin outer"}, [
        								m("div", {class:"inner_fixed"}, [			        						
	        								m("div", {class:"remove-margin container"}, [
			        	        				m("div", {class:"square round", style:"background-image: url('" + account["profileImage"] + "');"}, []),
				        						m("div", {class:""},[
				        							m("p", {class:"text-bold"}, account["pseudos"])
				        						]),
				        					]),
				        					m("div", {class:"divider"}),
				        				]),
			        					m("div", {class:""}, value.description),
			        					m("div", {class:"col 12 row remove-margin remove-padding"}, [
				        					m("div", {class:"divider col s12"}),
				        					m("div", {class:"col s12 row remove-margin remove-padding valign-wrapper"}, [
					        					m("a", {id:"modal"+index+"Like", class:"col s2 btn-floating btn-flat transparent remove-margin"}, [
					        					]),
					        					m("div", {id:"likeCount"+index, class:"col s2 remove-margin"}, [
					        					])
					        				])
				        				])
	        						])
	        					])
        					])
        				])
	    	        })   
	    	    ])
        	])
    }
}

async function isLikedBy(userId, postId, idLikeButton, idLikesCount)
{
	console.log("isLikedBy : " + userId, postId, idLikeButton)
	$.ajax({
		url : "/posts/" + postId + "/likes/" + userId,
        type : 'GET',
        dataType : 'html',
        success : async function(data, statut)
		{
        	let likesNumber = await getPostLikes(postId)
        	likesNumber = likesNumber.likesCount
        	console.log("Data : " + data)
        	console.log("likesNumber : " + likesNumber)
        	if(data !== "")
        	{
        		setUnlike(userId, postId, idLikeButton, idLikesCount, likesNumber)        		
        	}
        	else
        	{
        		setLike(userId, postId, idLikeButton, idLikesCount, likesNumber)	
        	}
		}
	})
}

function setLike(userId, postId, idLikeButton, idLikesCount, likesNumber)
{
	console.log("setLike : " + userId, postId, idLikeButton)
	let elementLike = document.getElementById(idLikeButton)
	elementLike.classList.remove("pulseTest")
	elementLike.setAttribute('onclick', `like("${userId}", "${postId}", "${idLikeButton}", "${idLikesCount}")`)
	
	var likeBlock = {
		view: function(vnode){
			return m('',
					m("i", {class:"Tiny material-icons", style:"color:black;"}, "favorite_border")
			)
		}
	}
	
	m.mount(elementLike, likeBlock)		
	refreshLikeNumber(idLikesCount, likesNumber)
}

function setUnlike(userId, postId, idLikeButton, idLikesCount, likesNumber)
{
	console.log("setUnLike : " + userId, postId, idLikeButton)
	let elementLike = document.getElementById(idLikeButton)
	elementLike.classList.add("pulseTest")
	elementLike.setAttribute('onclick', `unlike("${userId}", "${postId}", "${idLikeButton}", "${idLikesCount}")`)
		
	var unLikeBlock = {
		view: function(vnode){
			return m('',
					m("i", {class:"Tiny material-icons", style:"color:red;"}, "favorite")
			)
		}
	}

	m.mount(elementLike, unLikeBlock)
	refreshLikeNumber(idLikesCount, likesNumber)
}

function refreshLikeNumber(idLikesCount, likesNumber)
{
	var likesNumberBlock = {
		view: function(vnode){
			return m('',
				m("p", {class:"text-bold remove-margin"}, likesNumber)
			)
		}
	}
	m.mount(document.getElementById(idLikesCount), likesNumberBlock)
	
}


function like(userId, postId, idLikeButton, idLikesCount)
{

	console.log("Like : " + userId, postId, idLikeButton)
	$.ajax({
		url : "/posts/" + postId + "/likes",
        type : 'POST',
        data : "&userId=" + userId,
        dataType : 'html',
        success : async function(data, statut)
		{
        	console.log(data)
        	let likesNumber = await getPostLikes(postId)
		    setUnlike(userId, postId, idLikeButton, idLikesCount, likesNumber.likesCount)
		}
	})	
}

function unlike(userId, postId, idLikeButton, idLikesCount)
{
	console.log("Unlike : " + userId, postId, idLikeButton)
	$.ajax({
		url : "/posts/" + postId + "/likes/" + userId,
        type : 'DELETE',
        dataType : 'html',
        success : async function(data, statut)
		{
        	console.log(data)
        	let likesNumber = await getPostLikes(postId)
        	setLike(userId, postId, idLikeButton, idLikesCount, likesNumber.likesCount)
		}
	})
}



async function getPostLikes(postId)
{
	return new Promise((resolve, reject) => {
		$.ajax({
			url : '/posts/' + postId + '/likes/count',
	    	type : 'GET',
	    	dataType : 'html',
	    	success : async function(data, statut)
	    	{
	    		let likeNumber = JSON.parse(data)
	    	    resolve(likeNumber)
	    	},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
	    })
	})
}