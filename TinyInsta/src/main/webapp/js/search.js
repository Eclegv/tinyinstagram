var id = getCookie("userId");

var Search = {
    view: function() {
    	return m("div", {class:"row"}, [
    		m("div", {class:"col s12 row"}, [
    			m("div", {id:"resultsContainer", class:"col s10 offset-s2 row", style:""}, [
	    			accounts.map(function(value, index)
	        		{
	    				console.log(accounts)
	    				return m("ul", {class:"col s3 collection indigo lighten-5 z-depth-2", style:"margin:1%"}, [
	    					m("li", {class:"collection-item avatar indigo lighten-5"}, [
	    						m("a", {href:"/account/" + value["id"]}, [
	        						m("img", {class:"circle", src:value["profileImage"]}),
	    						]),
	    						m("a", {href:"/account/" + value["id"]}, [
	        						m("span", {class:"title"}, value["pseudos"])
	    						]),
	    						m("a", {href:"/account/" + value["id"], class:"secondary-content"}, [
	    							m("i", {class:"material-icons"}, "send")
	    						])
	    					])
	    				])
	        		})
	        	]),
	        	m("div", {id:"expandButton", class:"col s2 offset-s5 row"}, [
		        	m("a", {class:"waves-effect waves-light btn-small col s12", onclick: async function(){await loadNewResults()}}, [
		        		m("i", {class:"material-icons center"}, "expand_more")
		        	])
		        ])
    		])
    	])
    }
}