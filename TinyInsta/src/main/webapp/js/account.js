var Account = {
    view: function() {
    	return m("div", {class:"row"},  [
	        m("div", {class:"row col s12"},  [
	        	m("div", {class:"col s6 offset-s3"}, [
	        		m("div", {class:"card-panel"}, [
	        			m("div", {class:"row valign-wrapper"}, [
	        				m("div", {class:"square round", style:"background-image: url('" + account["profileImage"] + "');"}, []),
	                		m("div", {class:"col s9 row"}, [
	            				m("div", {class:"col s12"}, [
	            					m("h4", account["email"])
	            				]),
	            				m("ul", {class:"col s12 list-inline"}, [
	            					m("li", {class:""}, account.posts.postsCount + " Publications"),
	            					m("li", {class:"elementPadding"}, account.followers.followersCount + " Abonnés"),
	            					m("li", {class:"elementPadding"}, account.followees.followeesCount + " Abonnement")
	                			]),
	                			m("div", {class:"col s12 row valign-wrapper"}, [
		            				m("div", {style:"font-weight: bold;",class:"col s4 valign remove-padding remove-margin"}, account["pseudos"]),
		            				m("div", {id:"followButton", class:"col s6 remove-margin"}, [
		            					
		            				])
		            			])
	                		])
	        			])
	        		])
	        	])
        	]),
        	m(Posts)
        ])
    }
}

async function follow(id)
{
	console.log("follow")
	return new Promise((resolve, reject) => {
		$.ajax({
	        url : '/users/' + getCookie("userId") + '/followees',
	        type : 'POST',
	        data : "&followeeId=" + id,
	        dataType : 'html',
	        success : async function(data, statut)
			{
				console.log(data)

	        	let dataF = JSON.parse(data)

 				resolve(dataF)
	        },
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
		});
	});
}

async function unfollow(id)
{
	console.log("unfollow")
	return new Promise((resolve, reject) => {
		$.ajax({
	        url : '/users/' + getCookie("userId") + '/followees/' + id,
	        type : 'DELETE',
	        dataType : 'html',
	        success : async function(data, statut)
			{
				console.log(data)

	        	let dataF = JSON.parse(data)

 				resolve(dataF)
	        },
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
		});
	});
}

function setFollow()
{
	console.log("setFollow")
	var followBlock = {
		view: function(vnode){
			return m('',
					m("a", {class:"waves-effect waves-light btn-flat btn-large transparent remove-padding remove-margin", onclick: async function(){
						await follow(account.id)
						location.reload();
					}},[
        				m("i", {class:"black-text material-icons left"}, "add"),
					], "s'abonner")
			)
		}
	}
	m.mount(document.getElementById("followButton"), followBlock)
}


function setUnfollow()
{
	console.log("setUnfollow")
	var unfollowBlock = {
		view: function(vnode){
			return m('',
					m("a", {class:"waves-effect waves-light btn-flat btn-large transparent remove-padding remove-margin", onclick: async function(){
						await unfollow(account.id)
						location.reload();
					}},[
        				m("i", {class:"black-text material-icons left"}, "remove"),
					], "se desabonner")
			)
		}
	}
	m.mount(document.getElementById("followButton"), unfollowBlock)
}

