var id = getCookie("userId");
var UserInfo = getInfo();
var pseudo = "";
var profilePicture = "";
function getInfo()
{
	$.ajax({
        url : '/users/' + id ,
        type : 'get',
        dataType : 'html',
        success : function(data, statut)
        {
        	var jsonParsed = JSON.parse(data);	
        	$("#Nickname").val(jsonParsed["pseudos"]);
        	pseudo = jsonParsed["pseudos"];
        	profilePicture = jsonParsed["profileImage"];
        	$("#Image").val(profilePicture);
        	previewFile();
        },
      });
}

function updateInfo(){
	let pseudo = document.getElementById("Nickname").value
	let profilePicture = document.getElementById("Image").value
	
	$.ajax({
        url : '/users/' + id ,
        type : 'PUT',
        data : {pseudo:pseudo,image:profilePicture},
        dataType : 'html',
        success : function(data, statut)
        {
        	M.toast({html: 'Your name and profile picture were updated'})
        },
        error:function(jqXHR, textStatus){
        	M.toast({html: 'An error occured, profile not updated, error : ' + jqXHR.status})
       }
      });
}
function setSRC(src)
{
	var preview = document.querySelector('img');
	preview.src = src;
}


async function previewFile() {
	let loadedImage = await new Promise((resolve, reject) => {
	    let image = new Image()
	    image.onload = function () {
	        resolve(image)
	    }
	    image.onerror = function () {
	        reject(new Error('Failed to load image'))
			setSRC("");
	    }
	    image.src = document.querySelector('input[type=url]').value
	})
	setSRC(loadedImage.src);
}