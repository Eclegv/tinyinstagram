var ApplicationTests = {
    view: function() {
        return m("div", {class:"row"},  [
        	m("div", {class:"col s8 offset-s2 row"}, [
    			m("div", {class:"btn waves-effect waves-light col s3", style:"margin:1%", onclick: async () => {await createFollowers(10), alert("Followers crées")}}, "Creer 10 followers", [
    			]),
    			m("div", {class:"btn waves-effect waves-light col s3", style:"margin:1%", onclick: async () => {await createFollowers(100), alert("Followers crées")}}, "Creer 100 followers", [
    			]),
    			m("div", {class:"btn waves-effect waves-light col s3", style:"margin:1%", onclick: async () => {await createFollowers(500), alert("Followers crées")}}, "Creer 500 followers", [
    			]),
    			m("div", {class:"btn waves-effect waves-light col s2", style:"margin:1%", onclick: async () => {await createPosts(30, true)}}, "+ 30 Posts", [
        		]),
    			m("div", {class:"btn waves-effect waves-light col s3", style:"margin:1%", onclick: async () => {await createPosts(10, false)}}, "Creer 10 Posts", [
        		]),
        		m("div", {class:"btn waves-effect waves-light col s3", style:"margin:1%", onclick: async () => {await createPosts(100, false)}}, "Creer 100 Posts", [
        		]),
        		m("div", {class:"btn waves-effect waves-light col s3", style:"margin:1%", onclick: async () => {await createPosts(500, false)}}, "Creer 500 Posts", [
        		]),
        	])
        ])
    }
}

async function createFollowers(followersNumber)
{
    let usersList = await createRandomUsers(followersNumber)
    for(let user of usersList)
    {
    	let followR = await follow(user)
    }
}

async function createPosts(postsNumbers, isTimeMeasured)
{
	let start;
	let stop;
	let total=0;
	for(let i = 0; i < postsNumbers; i++)
	{
		if(isTimeMeasured)
		{
			start = new Date()
		}
		await createPost()
		if(isTimeMeasured)
		{
			stop = new Date()
			console.log(stop-start)
			total += stop-start 
		}
	}
	let tempsMoyen = total/postsNumbers
	console.log("Temps moyen : " + tempsMoyen + " ms")
}

async function createPost() {
	var userId = getCookie("userId");
	return new Promise((resolve, reject) => {
	    $.ajax({
	        url : '/users/' + userId + '/posts',
	        type : 'POST',
	        data: "&image=https://img-9gag-fun.9cache.com/photo/aGgLbo5_460swp.webp"+
		        "&description=TEST",
	        dataType : 'html',
	        success : function(data, statut)
	        {
	        	resolve()
	        },
	    })
	})
}

async function follow(id)
{
	console.log("follow")
	return new Promise((resolve, reject) => {
		$.ajax({
	        url : '/users/' + id + '/followees',
	        type : 'POST',
	        data : "&followeeId=" + getCookie("userId"),
	        dataType : 'html',
	        success : function(data, statut)
			{
 				resolve(JSON.parse(data))
	        },
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
		});
	});
}

async function createTests() {
    var userNumber = parseInt(document.getElementById('userNumber').value);
    var postNumber = parseInt(document.getElementById('postNumber').value);
    
    var usersList = await createRandomUsers(userNumber)
    console.log("Utilisateurs crées")
    console.log(usersList)
    
    var postsList = await createPostsForUsers(usersList, postNumber)
    console.log("Posts  crées")
    console.log(postsList)
    
    await likesRandomPosts(usersList, postsList);   
	window.location.replace("news");
    
}

async function likesRandomPosts(users, posts)
{
	usersNumber = users.length - 11
	let i = 1;
	for(let post of posts)
	{
		console.log("Post number : " + i)
		let start = Math.trunc((Math.random()*usersNumber))
		let stop = start + Math.trunc((Math.random()*10))
		let usersLiking = users.slice(start, stop)
		await usersLikesPost(usersLiking, post)
		i++
	}
}

async function usersLikesPost(users, post)
{
	let promises = []
	users.forEach((value) => {
		promises.push(
			new Promise((resolve, reject) => {
				$.ajax({
					url : '/posts/' + post + '/likes',
					type : 'POST',
					data:'&userId=' + value,
					dataType : 'html',
					success : function(data, status)
					{
						resolve(JSON.parse(data))
					},
					error : function(error, status)
					{
						reject(new Error(`${error}, ${status}`))
					}
				});
			})
		)
	})
	await Promise.all(promises)
}

function generateRandomMail()
{
	var chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
	var string = '';
	for(var ii=0; ii<15; ii++){
	    string += chars[Math.floor(Math.random() * chars.length)];
	}
	return string + '@test.com'	
}

function generateRandomString()
{
	return Math.random().toString(36).substring(2, 7) + Math.random().toString(36).substring(2, 7)
}

async function createPostsForUsers(usersList, postNumber)
{
	let posts = []
	for(let user of usersList)
	{
		posts = posts.concat(await createPostsForUser(user, postNumber))
	}
	return posts
}

async function createPostsForUser(user, postNumber)
{
	let promises = []
	for(let x = 0; x < postNumber; x++)
	{
		promises.push(
			new Promise((resolve, reject) => {
				$.ajax({
					url : '/users/' + user + '/posts',
					type : 'POST',
					data:'&image=https://moderndiplomacy.eu/wp-content/uploads/2018/08/uzbekistan-forests.jpg' + "&description=" + generateRandomString(),
					dataType : 'html',
					success : function(data, status)
					{
						resolve(JSON.parse(data).postId)
					},
					error : function(error, status)
					{
						reject(new Error(`${error}, ${status}`))
					}
				});
			})
		)
	}
	return await Promise.all(promises)
}

async function createRandomUsers(userNumber)
{
	let promises = []
	for(let x = 0; x < userNumber; x++)
	{
		promises.push(
			new Promise((resolve, reject) => {
				$.ajax({
					url : '/users',
					type : 'POST',
					data:'&email=' + generateRandomMail() + "&name=" + generateRandomString(),
					dataType : 'html',
					success : function(data, status)
					{
						resolve(JSON.parse(data).id)
					},
					error : function(error, status)
					{
						reject(new Error(`${error}, ${status}`))
					}
				});
			})
		)
	}
	return await Promise.all(promises)
}

function checkInput() {
    var userNumber = document.getElementById('userNumber').value;
    var postNumber = document.getElementById('postNumber').value;
    
    if(userNumber >= 1 && userNumber <= 10000 && postNumber >= 1 && postNumber <= 10000){    	
    	document.getElementById("sendNumber").classList.remove("disabled")
    }else{    	
    	document.getElementById("sendNumber").classList.add("disabled")
    }
}

function clearDatabase() {
	$.ajax({
		url : '/tests',
		type : 'DELETE',
		dataType : 'html',
		success : function(data, statut)
		{
			window.location.replace("news");
		},
	});
}