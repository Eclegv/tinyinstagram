function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

var Header = {
    view: function() {
        return m("nav", {class:"white row"},  [
        	m("div", {class:"nav-wrapper col s12 row"}, [
        		m("div", {class:"left col s1"}, [
        			m("a", {href:"/news", class:"blue-text brand-logo left"}, "TinyInsta")
        		]),
        		m("div", {class:"center col s4 offset-s3 row"}, [
        			m("div", {class:"col s12"}, [
            			m("div", {class:"row"}, [
            				m("form", {action: "/search", method: "get"}, [
                    			m("div", {class:"input-field col s8 s12 blue-text"}, [
                    				m("i", {class:"blue-text material-icons prefix"}, "search"),
                          			m("input", {type: "text", name:"query", placeholder: "Search"}),
                    			])
            				])
            			])
        			])
        		]),
        		m("ul", {class:"col s3 offset-s1 hide-on-med-and-down"}, [
        			m("li", [
            			m("a", {href: "/account/" + getCookie("userId")}, [
        					m("i", {class:"blue-text material-icons prefix"}, "person")
        				])        				
        			]),
        			m("li", [
            			m("a", {href: "/postCreation"}, [
        					m("i", {class:"blue-text material-icons prefix"}, "add_box")
        				])        				
        			]),
        			m("li", [
        				m("a", {href: "/option"
        				}, [
        					m("i", {class:"blue-text material-icons prefix"}, "settings_applications")
        				]),
            		]),
            		m("li", [
        				m("a", {href: "/applicationTests"
        				}, [
        					m("i", {class:"blue-text material-icons prefix"}, "warning")
        				]),
            		]),
        			m("li", [
        				m("a", {onclick: function(){
        					gapi.load('auth2', function() {
        				        gapi.auth2.init();
        					});
        					var auth2 = gapi.auth2.getAuthInstance();
        				    auth2.signOut().then(function () {
        				        console.log('User signed out.');
        				    });
        				    document.cookie = "userId=;";
        				    window.location.replace("/")
        				}},[
        					m("i", {class:"blue-text material-icons prefix"}, "keyboard_tab")
        				])
            		])
        		])
        	])  
        ])
    }
}