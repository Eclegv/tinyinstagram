var PostCreate = {
    view: function() {
        return m("div", {class:"row"},  [
        	m("div", {class:"col s6 offset-s3 row"}, [
        		m("div", {class:"card-panel col s12 row"}, [
        			m("div", {class:"input-field col s12"}, [
        				m("input", {type: "url", name: "image", id: "filechooser", oninput: previewFile}),
        				m("label", {for:"filechooser"}, "Lien de l'image"),
        				m("img", {src: "", height: "200"})
        			]),
        			m("div", {class:"input-field col s12"}, [
        				m("textarea", {class:"materialize-textarea", id:"description", name: "description"}),
        				m("label", {for:"description"}, "Description")        				
        			]),
    				m("div", {class:"btn waves-effect waves-light col s2 row disabled", id:"envoyerPost", onclick: createPost}, "Envoyer", [
    					m("i", {class:"material-icons right"}, "send")
    				])
        		])
        	])
        ])
    }
}

function createPost() {
	var userId = getCookie("userId");
    var image = document.querySelector('input[type=url]').value;
    var description = document.querySelector('textarea').value;
    $.ajax({
        url : '/users/' + userId + '/posts',
        type : 'POST',
        data: "&image=" + image +
	        "&description=" + description,
        dataType : 'html',
        success : function(data, statut)
        {
        	window.location.replace("news");
        },
    });
}

function setSRC(src)
{
	var preview = document.querySelector('img');
	preview.src = src;
}


async function previewFile() {
	let loadedImage = await new Promise((resolve, reject) => {
	    let image = new Image()
	    image.onload = function () {
	        resolve(image)
			document.getElementById("envoyerPost").classList.remove("disabled")
	    }
	    image.onerror = function () {
	        reject(new Error('Failed to load image'))
			document.getElementById("envoyerPost").classList.add("disabled")
			setSRC("");
	    }
	    image.src = document.querySelector('input[type=url]').value
	})
	setSRC(loadedImage.src);
}