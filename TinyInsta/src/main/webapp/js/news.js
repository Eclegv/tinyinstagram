var id = getCookie("userId");

var News = {
	view: function() {
		console.log(postsNews)
		return m("div", {class:"row"}, [
			m("div", {class:"col s6 offset-s3 row"},[
				postsNews.map((value, index) => {
					console.log(value)
					return m("div", {class:"card col s12", style:"overflow: visible;"}, [
						m("div", {class:"card-content"}, [
							m("a", {class:"card-title grey-text text-darken-4", href:"/account/" + value["user"].id}, value.user.pseudos)
						]),
						m("div", {class:"col s12 divider"}),
						m("div", {class:"card-image", style:"margin:1.66%;"}, [
							m("img", {src:value.image})
						])
					])
				})
			])
		])
	}
}
			
async function getRecentFolloweesPosts()
{
	return new Promise((resolve, reject) => {
		$.ajax({
			url : '/users/' + id + '/followees/posts',
			type : 'GET',
			data:"&page=1&limit=15",
			dataType : 'html',
			success : async function(data, statut)
			{
				let posts = JSON.parse(data)	
				resolve(posts)
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				reject(new Error("Status: " + textStatus))
			}
		});		
	})
}