function loadMaterialize()
{
	var elems = document.querySelectorAll('.modal');
	var instances = M.Modal.init(elems);
    elems = document.querySelectorAll('.materialboxed');
    instances = M.Materialbox.init(elems);
}

async function loadImg(data, key)
{
	return new Promise((resolve, reject) => {
	    let image = new Image()
	    image.onload = function () {
	        resolve(image)
	    }
	    image.onerror = function () {
	        reject(new Error('Failed to load image'))
	    }
	    image.src = data[key]
	})
}

async function getPosts(userId)
{
	return new Promise((resolve, reject) => {
		$.ajax({
	        url : '/users/' + userId + '/posts',
	        type : 'GET',
	        dataType : 'html',
	        success : async function(data, statut)
	        {
	        	let posts = JSON.parse(data)
	        	console.log(posts)
	        	for(let i = 0; i < posts.length; i++)
	        	{
	        		let loadedImage = await loadImg(posts[i], "image")
	        		posts[i].imageWidth = loadedImage.naturalWidth
	        		posts[i].imageHeight = loadedImage.naturalHeight
	        	}     	
 				resolve(posts)
	        },
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
		});
	});
}


async function getPostsNbr(userId)
{
	console.log("getPostsNbr");
	return new Promise((resolve, reject) => {
		$.ajax({
	        url : '/users/' + userId + '/posts/count',
	        type : 'GET',
	        dataType : 'html',
	        success : async function(data, statut)
			{
				console.log("Post Number : " + data)

	        	let postsNumbersData = JSON.parse(data)

 				resolve(postsNumbersData)
	        },
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
		});
	});
}

async function getFollowersNbr(userId)
{
	console.log("getFollowersNbr");
	return new Promise((resolve, reject) => {
		$.ajax({
	        url : '/users/' + userId + '/followers/count',
	        type : 'GET',
	        dataType : 'html',
	        success : async function(data, statut)
			{
				console.log("Followers Number : " + data)

	        	let followersNumbersData = JSON.parse(data)

 				resolve(followersNumbersData)
	        },
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
		});
	});
}

async function getFolloweesNbr(userId)
{
	console.log("getFolloweesNbr");
	return new Promise((resolve, reject) => {
		$.ajax({
	        url : '/users/' + userId + '/followees/count',
	        type : 'GET',
	        dataType : 'html',
	        success : async function(data, statut)
			{
				console.log("Followees Number : " + data)

	        	let followeesNumbersData = JSON.parse(data)

 				resolve(followeesNumbersData)
	        },
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
		});
	});
}

async function getUserDetails(userId)
{
	return new Promise((resolve, reject) => {
		$.ajax({
	        url : '/users/' + userId,
	        type : 'GET',
	        dataType : 'html',
	        success : async function(data, statut)
	        {
				let accountData = JSON.parse(data)
	        	
    			let loadedImage = await loadImg(accountData, "profileImage")  
	        	account.profileImageWidth = loadedImage.naturalWidth
				account.profileImageHeight = loadedImage.naturalHeight
				
				let postsNbrResults = getPostsNbr(userId);
				let followersNbrResults = getFollowersNbr(userId);
				let followeesNbrResults = getFolloweesNbr(userId);
				
				[accountData.posts,
				 accountData.followers,
				 accountData.followees] = [await postsNbrResults,
										   await followersNbrResults,
										   await followeesNbrResults];
				
				console.log(accountData)
 				resolve(accountData)
	        },
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
		});
	});
}

async function isFollowedBy(id)
{
	console.log("isFollowedBy")
	return new Promise((resolve, reject) => {
		$.ajax({
	        url : '/users/' + getCookie("userId") + '/followees/' + id,
	        type : 'GET',
	        dataType : 'html',
	        success : async function(data, statut)
			{
				console.log(data)
				if(data === "")
					setFollow()
				else
					setUnfollow()
	 			resolve(data)
	        },
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
                reject(new Error("Status: " + textStatus))
            }
		});
	});
}

async function mountPage()
{
	var App = {
		view: function(vnode){
			return m(''
				, m(Header)
			   	, m(Account)
			)
		}
	}
	m.mount(document.body, App)
	loadMaterialize()
}