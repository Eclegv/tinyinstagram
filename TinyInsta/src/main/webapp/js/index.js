var Index = {
	    view: function() {
	        return m("h1", "Hello app Engine!"),
            m("div", {class:"g-signin2", 'data-onsuccess':"onSignIn", 'data-theme':"dark"})
	    }
}

function onSignIn(googleUser){
    var profile = googleUser.getBasicProfile();
    $.ajax({
        url : '/users',
        type : 'POST',
        data : '&name=' + profile.getName() +
        		'&email=' + profile.getEmail(),
        dataType : 'html',
        success : function(data, statut)
        {    
        	console.log(data);
        	post = JSON.parse(data);
        	document.cookie = "userId="+post['id'];
        	window.location.replace('news');
        },
    });
}
