<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

	<head>
		<meta name="google-signin-client_id" content="477410515646-sei7khrt9ikbh49kr6r19fl9b2tq2irv.apps.googleusercontent.com">
  		<script src="https://unpkg.com/mithril/mithril.js"></script>
  		<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	</head>

 	<body>
 		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 		<script type="text/javascript" src="/js/header.js"></script>
 		<script type="text/javascript" src="/js/search.js"></script>
 		
		<script>
		
			var accounts = []
			var query = "${query}";
			var limit = 30;
			var page = 1;
			var usersNumber = 0;

	 		async function loadNewResults()
	 		{
	 			console.log("Page" + page)
	 			console.log("Result nbr" + accounts.length)
	 			let results = await loadAccountPerPage(page, limit)
	 			accounts = accounts.concat(results)
	 			loadInsideResultContainer()
	 			console.log("Result nbr" + accounts.length)
	 			
	 			console.log("Number of users : " + usersNumber)
	 			if(accounts.length === usersNumber)
	 			{
	 				deactivateExpand()
	 			}
	 		}
	 		
	 		function deactivateExpand()
	 		{
				let expandButton = document.getElementById("expandButton")
				var deactivatedButton = {
					view : function(vnode){
						return m("a", {class:"disabled waves-effect waves-light btn-small col s12", onclick: async function(){await loadNewResults()}}, [
			        		m("i", {class:"material-icons center"}, "expand_more")
				        ])
					}
				}
				m.mount(expandButton, deactivatedButton)	
	 		}
	
	 		async function loadAccountPerPage(pageNumber, limit)
	 		{
	 			console.log("loadAccountPerPage")
	 			console.log(pageNumber, limit)
	 			return new Promise((resolve, reject) => {
	 				$.ajax({
	 			        url : '/users',
	 			        type : 'GET',
	 			        data:"&searchQuery=" + query + "&page=" + page +  "&limit=" + limit,
	 			        dataType : 'html',
	 			        success : async function(data, statut)
	 			        {
	 			        	let res = JSON.parse(data)
	 			        	console.log("Request res : " + res.length)
	 			 			page++
	 		 				resolve(res)
	 			        },
	 					error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 		                reject(new Error("Status: " + textStatus))
	 		            }
	 				});
	 			})
	 		}

	 		function loadInsideResultContainer()
	 		{
	 			var App = {
	 				view : function(vnode) 
	 				{
	 					return m('', 
	 						m(Header),
	 						m(Search)
	 					)
	 				}
	 			}
	 			m.mount(document.body, App)
	 		}
	 		
	 		async function getUsersNumber()
	 		{
	 			return new Promise((resolve, reject) => {
	 				$.ajax({
	 			        url : '/users/count',
	 			        type : 'GET',
	 			        dataType : 'html',
	 			        success : async function(data, statut)
	 			        {
	 			        	let res = JSON.parse(data)
	 		 				resolve(parseInt(res.usersCount))
	 			        },
	 					error: function(XMLHttpRequest, textStatus, errorThrown) { 
	 		                reject(new Error("Status: " + textStatus))
	 		            }
	 				});
	 			})
	 		}
		
			
			async function getData()
			{			
				usersNumber = await getUsersNumber();
				loadNewResults()
			}

			getData()
		</script>
  	</body>
</html>
