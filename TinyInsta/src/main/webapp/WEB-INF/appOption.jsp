<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

	<head>
		<meta name="google-signin-client_id" content="477410515646-sei7khrt9ikbh49kr6r19fl9b2tq2irv.apps.googleusercontent.com">
  		<script src="https://unpkg.com/mithril/mithril.js"></script>
  		<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	</head>

 	<body>
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 	<script type="text/javascript" src="/js/header.js"></script>
 	<script type="text/javascript" src="/js/option.js"></script>
 	<script>
	var App = {
	   	view: function(vnode){
	   	    return m(''
	   	        ,m(Header)
		   	    ,m("div", {class:"row"}, [
	   	         	m("div",{class:"col s12 row"},[
			   	           	m("div",{class:"col s6 offset-s3 row"},[
			   	           		m("div",{class:"input-field col s12	"},[
			   	           			m("h6","General Information"),
			   	                    m("input", {type: "text",  placeholder: "Nickname",id:"Nickname",class:"Validate"}),
			   	                    m("h6","Account Customization"),
			   	                    m("input", {type: "url",  placeholder: "Image URL",id:"Image",name:"image",oninput: previewFile, class:"Validate"}),
			   	         			m("img", {src: "", height: "200"})
			   		            ]),
			   		         m("button", {class: "waves-effect waves-light blue btn",onclick:updateInfo}, "Accept")
			   		       ])
		   		       ])
	  		    	])
	   		    )
	   		}
	   	}
	   	m.mount(document.body, App)
    	</script>
  	</body>
</html>
