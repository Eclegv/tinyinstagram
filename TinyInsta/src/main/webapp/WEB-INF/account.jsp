<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

	<head>
		<meta charset="UTF-8">
		<meta name="google-signin-client_id" content="477410515646-sei7khrt9ikbh49kr6r19fl9b2tq2irv.apps.googleusercontent.com">
  		<script src="https://unpkg.com/mithril/mithril.js"></script>
  		<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    	<link rel="stylesheet" href="/css/style.css">
	</head>

 	<body>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 		<script type="text/javascript" src="/js/header.js"></script>
 		<script type="text/javascript" src="/js/accountFunctions.js"></script>
 		<script type="text/javascript" src="/js/posts.js"></script> 		
 		<script type="text/javascript" src="/js/account.js"></script>
 		
   		<script>
			var userID = "${userID}";
			var post = []
			var account = []
			
			async function getData()
			{
				let start = new Date();
				const postsResults = await getPosts(userID);
				let stop = new Date();
				let total = stop-start
				console.log("Temps : " + total + " ms")
				const accountResults = getUserDetails(userID);

				[post, account] = [await postsResults, await accountResults];
				mountPage();
				isFollowedBy(account.id)
			}
			getData()
			
    	</script>
  	</body>
</html>