package database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.search.DateUtil;

public class Post extends JSONObject{
		
	private String userId;
	private String image;
	private String description;
	private Date date;
	private Key key;
	
	// CONSTRUCTOR and GENERIC METHODE
	
	public Post(String userId, String image, String description) {
		this.key = KeyFactory.createKey("Post", UUID.randomUUID().toString());
		this.image = image;
		this.userId = userId;
		this.description = description;
		this.date = new Date();
	}
	
	public Post(Entity entity) {
		  this.key = entity.getKey();
		  this.userId = ""+entity.getProperty("userId");
		  this.image = ""+entity.getProperty("image");
		  this.description = ""+entity.getProperty("description");
		  this.date = (Date)(entity.getProperty("date"));
	}
	
	
	private Entity toEntity() {
		Entity post = new Entity(key);
		post.setProperty("userId", userId);
		post.setProperty("image", image);
		post.setProperty("description", description);
		post.setProperty("date", date);
		return post;
	}
	
	public String toJSON()
	{
		return "{ "
				+ "\"userId\": " + "\"" + this.userId + "\","
				+ "\"postId\": " + "\"" + this.key.getName() + "\","
				+ "\"image\": " + "\"" + this.image  + "\","
				+ "\"description\": "  + "\"" + this.description + "\","
				+ "\"date\": "  + "\"" + DateUtil.formatDateTime(this.date) + "\""
				+ "}";
	}
	
	public String toJSONWithUser()
	{
		User user;
		try {
			user = User.getById(userId);
		} catch (EntityNotFoundException e) {
			user = null;
			e.printStackTrace();
		}
		if(user == null) {
			return toJSON();
		}
		return "{ "
				+ "\"userId\": " + "\"" + this.userId + "\","
				+ "\"postId\": " + "\"" + this.key.getName() + "\","
				+ "\"image\": " + "\"" + this.image  + "\","
				+ "\"description\": "  + "\"" + this.description + "\","
				+ "\"date\": " + "\"" + DateUtil.formatDateTime(this.date) + "\","
				+ "\"user\": " + user.toJSON()
				+ "}";
	}

	// GET and SET
	
	public String getId() {
		return key.getName();
	}
	
	public String getImage() {
		return image;
	}
	
	public String getDescritpion() {
		return description;
	}
	
	public Key getKey() {
		return key;
	}
	
	// GENERIC DATABASE METHODES
	
	public static List<Post> getByUserId(String userId, int offset, int limit) {
		Query q = new Query("Post").setFilter(new FilterPredicate("userId", FilterOperator.EQUAL, userId))
								   .addSort("date", SortDirection.DESCENDING);
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		
		FetchOptions fetchOption = FetchOptions.Builder.withDefaults();
		if(limit > 0) {
			fetchOption.limit(limit);
		}
		if(offset > 0) {
			fetchOption.offset(offset);
		}
		
		List<Entity> result = pq.asList(fetchOption);
		List<Post> posts = new ArrayList<Post>();
		for(Entity e : result) {
			posts.add(new Post(e));
		}
		return posts;
	}
	
	public static int countByUserId(String userId) {
		Query q = new Query("Post").setFilter(new FilterPredicate("userId", FilterOperator.EQUAL, userId));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		int result = pq.countEntities(FetchOptions.Builder.withDefaults());
		return result;
	}
	
	public static List<Post> getByUserIdList(List<String> ids, int offset, int limit){
		Query q = new Query("Post").setFilter(new FilterPredicate("userId", FilterOperator.IN, ids))
				   .addSort("date", SortDirection.DESCENDING);
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		
		FetchOptions fetchOption = FetchOptions.Builder.withDefaults();
		if(limit > 0) {
			fetchOption.limit(limit);
		}
		if(offset > 0) {
			fetchOption.offset(offset);
		}
		
		List<Entity> result = pq.asList(fetchOption);
		List<Post> posts = new ArrayList<Post>();
		for(Entity e : result) {
			posts.add(new Post(e));
		}
		return posts;
	}
	
	public static void save(Post post) {
		ServiceAccess.datastore.put(post.toEntity());
	}
	
	public static void remove(Post post) {
		ServiceAccess.datastore.delete(post.getKey());
	}
	
}
