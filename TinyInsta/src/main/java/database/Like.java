package database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

public class Like extends JSONObject{
	
	private String userId;
	private String postId;
	private Key key;
	
	// CONSTRUCTOR and GENERIC METHODE
	
	public Like(String userId, String postId) {
		this.key = KeyFactory.createKey("Like", UUID.randomUUID().toString());
		this.userId = userId;
		this.postId = postId;
	}
	
	public Like(Entity entity) {
		  this.key = entity.getKey();
		  this.userId = ""+entity.getProperty("userId");
		  this.postId = ""+entity.getProperty("postId");
	}
	
	private Entity toEntity() {
		Entity like = new Entity(key);
		like.setProperty("userId", userId);
		like.setProperty("postId", postId);
		return like;
	}
		
	public String toJSON()
	{
		return "{ "
				+ "\"userId\": " + "\"" + this.userId + "\","
				+ "\"postId\": " + "\"" + this.postId  + "\""
				+ "}";
	}
	
	// GET and SET
	
	public Key getKey() {
		return this.key;
	}
	
	public User getUser() throws EntityNotFoundException {
		return User.getById(userId);
	}
	
	// GENERIC DATABASE METHODES
	
	public static Like getByUserIdAndPostId(String userId, String postId) {
		Query q = new Query("Like").setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
	            new FilterPredicate("userId", FilterOperator.EQUAL, userId),
	            new FilterPredicate("postId", FilterOperator.EQUAL, postId)
	        )));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		Entity result = pq.asSingleEntity();
		Like like = null;
		if(result != null)
			like = new Like(result);
		return like;
	}
	
	public static List<Like> getByPostId(String postId, int offset, int limit) {
		Query q = new Query("Like").setFilter(new FilterPredicate("postId", FilterOperator.EQUAL, postId));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		
		FetchOptions fetchOption = FetchOptions.Builder.withDefaults();
		if(limit > 0) {
			fetchOption.limit(limit);
		}
		if(offset > 0) {
			fetchOption.offset(offset);
		}
		
		List<Entity> result = pq.asList(fetchOption);
		List<Like> likes = new ArrayList<Like>();
		for(Entity e : result) {
			likes.add(new Like(e));
		}
		return likes;
	}

	public static List<Like> getByUserId(String userId, int offset, int limit) {
		Query q = new Query("Like").setFilter(new FilterPredicate("userId", FilterOperator.EQUAL, userId));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		
		FetchOptions fetchOption = FetchOptions.Builder.withDefaults();
		if(limit > 0) {
			fetchOption.limit(limit);
		}
		if(offset > 0) {
			fetchOption.offset(offset);
		}
		
		List<Entity> result = pq.asList(fetchOption);
		List<Like> likes = new ArrayList<Like>();
		for(Entity e : result) {
			likes.add(new Like(e));
		}
		return likes;
	}
	
	public static int countByPostId(String postId) {
		Query q = new Query("Like").setFilter(new FilterPredicate("postId", FilterOperator.EQUAL, postId));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		int result = pq.countEntities(FetchOptions.Builder.withDefaults());
		return result;
	}
	
	public static void save(Like like) {
		ServiceAccess.datastore.put(like.toEntity());
	}
	
	public static void remove(Like like) {
		ServiceAccess.datastore.delete(like.getKey());
	}
	
	public static List<User> getPostLikeUsers(String postId, int offset, int limit) {
		List<Like> likes = getByPostId(postId, offset, limit);
		List<User> users = new ArrayList<User>();
		for(Like like: likes) {
			try {
				users.add(like.getUser());
			} catch (EntityNotFoundException e) {
				e.printStackTrace();
			}
		}
		return users;
	}

}
