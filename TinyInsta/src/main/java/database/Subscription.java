package database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

public class Subscription extends JSONObject{
	
	private String followerId;
	private String followeeId;
	private Key key;
	
	// CONSTRUCTOR and GENERIC METHODE
	
	public Subscription(String followerId, String followeeId) {
		this.key = KeyFactory.createKey("Subscription", UUID.randomUUID().toString());
		this.followerId = followerId;
		this.followeeId = followeeId;
	}
	
	public Subscription(Entity entity) {
		  this.key = entity.getKey();
		  this.followerId = ""+entity.getProperty("followerId");
		  this.followeeId = ""+entity.getProperty("followeeId");
	}
	
	private Entity toEntity() {
		Entity sub = new Entity(key);
		sub.setProperty("followerId", followerId);
		sub.setProperty("followeeId", followeeId);
		return sub;
	}
		
	public String toJSON()
	{
		return "{ "
				+ "\"followerId\": " + "\"" + this.followerId + "\","
				+ "\"followeeId\": " + "\"" + this.followeeId  + "\""
				+ "}";
	}
	
	// GET and SET
	
	public String getFolloweeId() {
		return followeeId;
	}
	
	public User getFollower() throws EntityNotFoundException {
		return User.getById(followerId);
	}
	
	public User getFollowee() throws EntityNotFoundException {
		return User.getById(followeeId);
	}
	
	public Key getKey() {
		return key;
	}
	
	// GENERIC DATABASE METHODES
	
	public static List<Subscription> getByFollowerId(String followerId, int offset, int limit) {
		Query q = new Query("Subscription").setFilter(new FilterPredicate("followerId", FilterOperator.EQUAL, followerId));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		
		FetchOptions fetchOption = FetchOptions.Builder.withDefaults();
		if(limit > 0) {
			fetchOption.limit(limit);
		}
		if(offset > 0) {
			fetchOption.offset(offset);
		}
		
		List<Entity> result = pq.asList(fetchOption);
		List<Subscription> subs = new ArrayList<Subscription>();
		for(Entity e : result) {
			subs.add(new Subscription(e));
		}
		return subs;
	}
	
	public static List<Subscription> getByFolloweeId(String followeeId, int offset, int limit) {
		Query q = new Query("Subscription").setFilter(new FilterPredicate("followeeId", FilterOperator.EQUAL, followeeId));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		
		FetchOptions fetchOption = FetchOptions.Builder.withDefaults();
		if(limit > 0) {
			fetchOption.limit(limit);
		}
		if(offset > 0) {
			fetchOption.offset(offset);
		}
		
		List<Entity> result = pq.asList(fetchOption);
		List<Subscription> subs = new ArrayList<Subscription>();
		for(Entity e : result) {
			subs.add(new Subscription(e));
		}
		return subs;
	}
	
	public static Subscription getByFollowerIdAndFolloweeId(String followerId, String followeeId) {
		Query q = new Query("Subscription").setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
	            new FilterPredicate("followerId", FilterOperator.EQUAL, followerId),
	            new FilterPredicate("followeeId", FilterOperator.EQUAL, followeeId)
	        )));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		Entity result = pq.asSingleEntity();
		Subscription sub = null;
		if(result != null)
			sub = new Subscription(result);
		return sub;
	}
	
	public static int countByFollowerId(String followerId) {
		Query q = new Query("Subscription").setFilter(new FilterPredicate("followerId", FilterOperator.EQUAL, followerId));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		int result = pq.countEntities(FetchOptions.Builder.withDefaults());
		return result;
	}

	public static int countByFolloweeId(String followeeId) {
		Query q = new Query("Subscription").setFilter(new FilterPredicate("followeeId", FilterOperator.EQUAL, followeeId));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		int result = pq.countEntities(FetchOptions.Builder.withDefaults());
		return result;
	}
	
	public static void save(Subscription sub) {
		ServiceAccess.datastore.put(sub.toEntity());
	}
	
	public static void remove(Subscription sub) {
		ServiceAccess.datastore.delete(sub.getKey());
	}
	
	public static List<User> getUserFollowers(String userId, int offset, int limit) {
		List<Subscription> subs = getByFolloweeId(userId, offset, limit);
		List<User> followers = new ArrayList<User>();
		for(Subscription sub : subs) {
			try {
				followers.add(sub.getFollower());
			} catch (EntityNotFoundException e) {
				e.printStackTrace();
			}
		}
		return followers;
	}

	public static List<User> getUserFollowees(String userId, int offset, int limit) {
		List<Subscription> subs = getByFollowerId(userId, offset, limit);
		List<User> followees = new ArrayList<User>();
		for(Subscription sub : subs) {
			try {
				followees.add(sub.getFollowee());
			} catch (EntityNotFoundException e) {
				e.printStackTrace();
			}
		}
		return followees;
	}
}
