package database;

import java.util.List;

public abstract class JSONObject {
	
	public abstract String toJSON();
	
	public static String listToJSON(List<? extends JSONObject> list) {
		String json = "[";
		for(JSONObject jo : list) {
			json += jo.toJSON();
			if(jo != list.get(list.size()-1)) {
				json += ",";
			}
		}
		json += "]";
		return json;
	}
	
	public static String listToJSONWithUser(List<? extends Post> list) {
		String json = "[";
		for(Post jo : list) {
			json += jo.toJSONWithUser();
			if(jo != list.get(list.size()-1)) {
				json += ",";
			}
		}
		json += "]";
		return json;
	}
	
	public static String intToJSON(String parameterName, int value) {
		return "{\"" + parameterName + "\":\"" + value + "\"}";
	}
	
}
