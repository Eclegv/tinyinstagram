package database;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

public class User extends JSONObject{

	private String pseudo;
	private String email;
	private String image;
	private Key key;

	// CONSTRUCTOR and GENERIC METHODE

	public User(String email, String pseudo) {
		this.key = KeyFactory.createKey("Users", UUID.randomUUID().toString());
		this.email = email;
		this.pseudo = pseudo;
		this.image = "https://images-ext-2.discordapp.net/external/lVPl1iuybi5UYOeEnF4Cws3vUPP6eWVzfkBLn-P-8Vo/http/s3.amazonaws.com/37assets/svn/765-default-avatar.png";
	}

	public User(Entity entity) {
		this.key = entity.getKey();
		this.pseudo = ""+entity.getProperty("pseudo");
		this.email = ""+entity.getProperty("email");
		this.image = ""+entity.getProperty("profileImage");
	}

	private Entity toEntity() {
		Entity user = new Entity(key);
		user.setProperty("pseudo", pseudo);
		user.setProperty("email", email);
		user.setProperty("profileImage", image);
		return user;
	}


	public String toJSON()
	{
		String json = "{"
				+ "\"id\":\""+ this.key.getName() + "\""
				+ " , \"email\":\""+ this.email + "\""
				+ " , \"pseudos\":\"" + this.pseudo + "\""
				+ " , \"profileImage\":\"" + this.image + "\""
				+ " }";
		return json;
	}

	// GET and SET

	public String getId() {
		return key.getName();
	}
	
	public String getEmail() {
		return email;
	}

	public String getPseudo() {
		return pseudo;
	}

	public String getProfileImage() {
		return image;
	}

	public Key getKey() {
		return key;
	}
	
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public void setProfileImage(String profileImage) {
		this.image = profileImage;
	}

	// GENERIC DATABASE METHODES

	public static int count() {
		Query q = new Query("Users");
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		return pq.countEntities(FetchOptions.Builder.withDefaults());
	}
	
	public static List<User> getUsers(String searchQuery, int offset, int limit)
	{
		Query q = new Query("Users");
		if (searchQuery != null && !searchQuery.equals("")) {
			q.setFilter(new FilterPredicate("pseudo", FilterOperator.EQUAL, searchQuery));
		}
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		
		FetchOptions fetchOption = FetchOptions.Builder.withDefaults();
		if(limit > 0) {
			fetchOption.limit(limit);
		}
		if(offset > 0) {
			fetchOption.offset(offset);
		}
		
		List<Entity> result = pq.asList(fetchOption);
		List<User> users = new ArrayList<User>();

		for(Entity e : result) {
			users.add(new User(e));
		}
		return users;
	}

	public static User getByEmail(String userEmail) {
		Query q = new Query("Users").setFilter(new FilterPredicate("email", FilterOperator.EQUAL, userEmail));
		PreparedQuery pq = ServiceAccess.datastore.prepare(q);
		List<Entity> result = pq.asList(FetchOptions.Builder.withDefaults());
		return ((result.size() > 0)? new User(result.get(0)) : null);
	}

	public static User getById(String id) throws EntityNotFoundException {
		Key newKey = KeyFactory.createKey("Users", id);
		Entity user = ServiceAccess.datastore.get(newKey);
		return new User(user);
	}

	public static void save(User user) {
		ServiceAccess.datastore.put(user.toEntity());
	}

	public static void remove(User user) {
		ServiceAccess.datastore.delete(user.getKey());
	}
	
}
