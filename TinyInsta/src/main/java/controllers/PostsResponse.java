package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.JSONObject;
import database.Post;
import database.Subscription;

public class PostsResponse {

	public static void retrieveUserPostsList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		String requestPage = request.getParameter("page");
		String requestLimit = request.getParameter("limit");
		int limit = 0;
		int offset = 0;
		if(requestLimit != null) {
			try {
				limit = Integer.parseInt(requestLimit);
			} catch(Exception e) {
				e.printStackTrace();
				limit = 0;
			}
			if(requestPage != null && limit != 0) {
				try {
					int page = Integer.parseInt(requestPage);
					offset = (page - 1)*limit;
				} catch(Exception e) {
					e.printStackTrace();
					offset = 0;
				}
			}
		}
		List<Post> posts = Post.getByUserId(userId, offset, limit);
		response.getWriter().write(JSONObject.listToJSON(posts));
	}
	
	public static void countUserPosts(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		int postsNb = Post.countByUserId(userId);
		response.getWriter().write(JSONObject.intToJSON("postsCount", postsNb));
	}
	
	public static void retrieveUserFolloweesPostsList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		String requestPage = request.getParameter("page");
		String requestLimit = request.getParameter("limit");
		int limit = 0;
		int offset = 0;
		if(requestLimit != null) {
			try {
				limit = Integer.parseInt(requestLimit);
			} catch(Exception e) {
				e.printStackTrace();
				limit = 0;
			}
			if(requestPage != null && limit != 0) {
				try {
					int page = Integer.parseInt(requestPage);
					offset = (page - 1)*limit;
				} catch(Exception e) {
					e.printStackTrace();
					offset = 0;
				}
			}
		}
		List<Subscription> subs = Subscription.getByFollowerId(userId, 0, 0);
		List<String> ids = new ArrayList<>();
		for(Subscription sub : subs) {
			ids.add(sub.getFolloweeId());
		}
		List<Post> posts;
		if(ids.size() > 0) {
			posts = Post.getByUserIdList(ids, offset, limit);
		} else {
			posts = new ArrayList<>();
		}
		response.getWriter().write(JSONObject.listToJSONWithUser(posts));
	}
	
	public static void createPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		String image = request.getParameter("image");
		String description = request.getParameter("description");
		Post post  = new Post(userId, image, description);
		Post.save(post);
		response.getWriter().write(post.toJSON());
	}
	
}
