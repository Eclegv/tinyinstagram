package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.JSONObject;
import database.Subscription;
import database.User;

public class SubscriptionsResponse {

	public static void retrieveUserFollowersList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		String requestPage = request.getParameter("page");
		String requestLimit = request.getParameter("limit");
		int limit = 0;
		int offset = 0;
		if(requestLimit != null) {
			try {
				limit = Integer.parseInt(requestLimit);
			} catch(Exception e) {
				e.printStackTrace();
				limit = 0;
			}
			if(requestPage != null && limit != 0) {
				try {
					int page = Integer.parseInt(requestPage);
					offset = (page - 1)*limit;
				} catch(Exception e) {
					e.printStackTrace();
					offset = 0;
				}
			}
		}
		List<User> followers = Subscription.getUserFollowers(userId, offset, limit);
		response.getWriter().write(JSONObject.listToJSON(followers));
	}
	
	public static void retrieveUserFolloweesList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		String requestPage = request.getParameter("page");
		String requestLimit = request.getParameter("limit");
		int limit = 0;
		int offset = 0;
		if(requestLimit != null) {
			try {
				limit = Integer.parseInt(requestLimit);
			} catch(Exception e) {
				e.printStackTrace();
				limit = 0;
			}
			if(requestPage != null && limit != 0) {
				try {
					int page = Integer.parseInt(requestPage);
					offset = (page - 1)*limit;
				} catch(Exception e) {
					e.printStackTrace();
					offset = 0;
				}
			}
		}
		List<User> followees = Subscription.getUserFollowees(userId, offset, limit);
		response.getWriter().write(JSONObject.listToJSON(followees));
	}
	
	public static void countUserFollowers(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		int followersNb = Subscription.countByFolloweeId(userId);
		response.getWriter().write(JSONObject.intToJSON("followersCount", followersNb));
	}

	public static void countUserFollowees(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		int followeesNb = Subscription.countByFollowerId(userId);
		response.getWriter().write(JSONObject.intToJSON("followeesCount", followeesNb));
	}
	
	public static void retrieveSubscription(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		String followeeId = pathInformations[2];
		Subscription sub = Subscription.getByFollowerIdAndFolloweeId(userId, followeeId);
		if(sub != null) {			
			response.getWriter().write(sub.toJSON());
		}
	}
	
	public static void createSubscription(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		String followeeId = request.getParameter("followeeId");
		Subscription sub = Subscription.getByFollowerIdAndFolloweeId(userId, followeeId);
		if(sub == null) {			
			sub = new Subscription(userId, followeeId);
			Subscription.save(sub);
		}
		response.getWriter().write(sub.toJSON());
	}
	
	public static void removeSubscription(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String followerId = pathInformations[0];
		String followeeId = pathInformations[2];
		Subscription sub = Subscription.getByFollowerIdAndFolloweeId(followerId, followeeId);
		Subscription.remove(sub);
		response.getWriter().write(sub.toJSON());
	}
	
}
