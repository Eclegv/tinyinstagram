package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "Posts",
    urlPatterns = {"/posts/*"}
)

public class PostController extends HttpServlet {

	//GET REQUESTS

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		//Some requests like /users/{id} and /users/{id}/posts/count will all be catch by this controller.
		//So we need to redirect the request depend of path informations
		String path = request.getPathInfo();
		if(path != null) {
			path = path.substring(1, path.length());
			String[] pathInformations = path.split("/");
			getPostsRedirection(request, response, pathInformations);
		}
	}

	private void getPostsRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		// This redirection will evaluate the first argument after '/users/' in path informations
		// So : `case "count" : ... ; break;` will recognize the path '/users/count...'
		// If the first argument is an id, is value isn't defined in advance, so we use `default` to catch all possibilities
		// In any case, the action will be different depend of the number of argument :
		// '/users/{id}' will not call the same function that '/users/{id}/posts'.
		switch (pathInformations[0]) {
		 	default :
		 		if(pathInformations.length > 1) {
		 			getPostsIdRedirection(request, response, pathInformations);
		 		}
		 		break;
		 }
	}

	//REDIRECTION LEVEL 2

	private void getPostsIdRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		// This redirection will evaluate the first argument after '/users/{id}' in path informations
		// Same principal than before
		switch (pathInformations[1]) {
			case "likes":
				if(pathInformations.length == 2) {
					LikesResponse.retrievePostLikedUsersList(request, response);
				} else {
		 			getPostsIdLikeRedirection(request, response, pathInformations);
				}
				break;
		}
	}

	//REDIRECTION LEVEL 3

		private void getPostsIdLikeRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
			// This redirection will evaluate the first argument after '/users/{id}' in path informations
			// Same principal than before
			switch (pathInformations[2]) {
				case "count":
					if(pathInformations.length == 3) {
						LikesResponse.countPostLikes(request, response);
					}
					break;
				default :
					if(pathInformations.length == 3) {
						LikesResponse.retrieveLike(request, response);
					}
					break;
			}
		}

	//POST REQUESTS

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// Same redirection system than get requests, see upper
		String path = request.getPathInfo();
		if(path != null) {
			path = path.substring(1, path.length());
			String[] pathInformations = path.split("/");
			postPostsRedirection(request, response, pathInformations);
		}
	}

	private void postPostsRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		 switch (pathInformations[0]) {
		 	default :
		 		if(pathInformations.length > 1) {
		 			postPostsIdRedirection(request, response, pathInformations);
		 		}
		 		break;
		 }
	}

	//REDIRECTION LEVEL 2

	private void postPostsIdRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		 switch (pathInformations[1]) {
		 	case "likes":
		 		if(pathInformations.length == 2) {
		 			LikesResponse.createLike(request, response);
		 		}
		 		break;
		 }
	}

	//DELETE REQUESTS

	public void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		//Some requests like /users/{id} and /users/{id}/posts/count will all be catch by this controller.
		//So we need to redirect the request depend of path informations
		String path = request.getPathInfo();
		if(path != null) {
			path = path.substring(1, path.length());
			String[] pathInformations = path.split("/");
			deletePostsRedirection(request, response, pathInformations);
		}
	}

	private void deletePostsRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		// This redirection will evaluate the first argument after '/users/' in path informations
		// So : `case "count" : ... ; break;` will recognize the path '/users/count...'
		// If the first argument is an id, is value isn't defined in advance, so we use `default` to catch all possibilities
		// In any case, the action will be different depend of the number of argument :
		// '/users/{id}' will not call the same function that '/users/{id}/posts'.
		switch (pathInformations[0]) {
		 	default :
		 		if(pathInformations.length > 1) {
		 			deletePostsIdRedirection(request, response, pathInformations);
		 		}
		 		break;
		 }
	}

	//REDIRECTION LEVEL 2

	private void deletePostsIdRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		// This redirection will evaluate the first argument after '/users/{id}' in path informations
		// Same principal than before
		switch (pathInformations[1]) {
			case "likes":
				if(pathInformations.length > 2) {
					LikesResponse.removeLike(request, response);
				}
				break;
		}
	}

}
