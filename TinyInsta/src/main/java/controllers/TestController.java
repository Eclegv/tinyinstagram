package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Like;
import database.Post;
import database.Subscription;
import database.User;

@WebServlet(
    name = "Tests",
    urlPatterns = {"/tests"}
)

public class TestController extends HttpServlet {

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String numberString = request.getParameter("number");
		int number = 0;
		try {
			number = Integer.parseInt(numberString);
		}catch(Exception e) {
			number = 0;
			e.printStackTrace();
		}
		List<User> users = new ArrayList<>();
		for(int i = 0; i < number; i++) {
			String name = randomString(10);
			User user = new User(name + "@test.com", name);
			User.save(user);
			for(User u : users) {
				Subscription.save(new Subscription(user.getId(), u.getId()));
			}
			users.add(user);
		}
		System.out.println("Tests users created");
		while(number >= 0) {
			for(int i = 0; i < number; i++) {
				User user = users.get(i);
				Post post = new Post(user.getId(), "https://s1.qwant.com/thumbr/0x380/0/d/272e53d9e1c7a39f7e6f35cc6603ad1808f170e9d857e810c989cebbb95b34/48631wide.jpg?u=https%3A%2F%2Fwallpaperstock.net%2Fwallpapers%2Fthumbs1%2F48631wide.jpg", randomString(5) + " " + randomString(10) + " " + randomString(2) + " " + randomString(7) + " " + randomString(3) + ".");
				Post.save(post);
				if(number == i+1) {
					for(int j = i+1; j < users.size(); j++) {
						Like like = new Like(users.get(j).getId(), post.getId());
						Like.save(like);
					}
				}
			}
			number--;
		}
		System.out.println("Tests likes created");
 	}

	public void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List<User> users = User.getUsers(null, 0, 0);
		for(User user : users) {
			List<Subscription> subs = Subscription.getByFolloweeId(user.getId(), 0, 0);
			for(Subscription sub : subs) {
				Subscription.remove(sub);
			}
			subs = Subscription.getByFollowerId(user.getId(), 0, 0);
			for(Subscription sub : subs) {
				Subscription.remove(sub);
			}
			List<Like> likes = Like.getByUserId(user.getId(), 0, 0);
			for(Like like : likes) {
				Like.remove(like);
			}
			List<Post> posts = Post.getByUserId(user.getId(), 0, 0);
			for(Post post : posts) {
				likes = Like.getByPostId(post.getId(), 0, 0);
				for(Like like : likes) {
					Like.remove(like);
				}
				Post.remove(post);
			}
			User.remove(user);
		}
	}
	
	private static String randomString(int n) { 
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvxyz"; 
        StringBuilder sb = new StringBuilder(n); 
        for (int i = 0; i < n; i++) { 
            int index = (int)(AlphaNumericString.length() * Math.random()); 
            sb.append(AlphaNumericString.charAt(index)); 
        } 
        return sb.toString(); 
    } 

}
