package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.EntityNotFoundException;

import database.JSONObject;
import database.User;

public class UsersResponse {

	public static void countUsers(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int nbUsers = User.count();
		response.getWriter().write(JSONObject.intToJSON("usersCount", nbUsers));
	}
	
	public static void retrieveUsersList(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		String searchQuery = request.getParameter("searchQuery");
		String requestPage = request.getParameter("page");
		String requestLimit = request.getParameter("limit");
		int limit = 0;
		int offset = 0;
		if(requestLimit != null) {
			try {
				limit = Integer.parseInt(requestLimit);
			} catch(Exception e) {
				e.printStackTrace();
				limit = 0;
			}
			if(requestPage != null && limit != 0) {
				try {
					int page = Integer.parseInt(requestPage);
					offset = (page - 1)*limit;
				} catch(Exception e) {
					e.printStackTrace();
					offset = 0;
				}
			}
		}
		List<User> users = new ArrayList<User>();
		users = User.getUsers(searchQuery, offset, limit);
		response.getWriter().write(JSONObject.listToJSON(users));
	}
	
	public static void retrieveUser(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		try {
			User user = User.getById(userId);
			String json = "";
			if(user != null) {
				json += user.toJSON();
			}
			response.getWriter().write(json);
		} catch(EntityNotFoundException e) {
			throw new ServletException(e.getMessage());
		}
	}
	
	public static void createUser(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String email = request.getParameter("email");
		String pseudo = request.getParameter("name");
		User user = User.getByEmail(email);
		if(user == null) {
			user = new User(email, pseudo);
			User.save(user);
		}
		response.getWriter().write(user.toJSON());
	}
	
	public static void modifyUser(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String userId = pathInformations[0];
		try {
			User user = User.getById(userId);
			String pseudo = request.getParameter("pseudo");
			String image = request.getParameter("image");
			if(pseudo != null) {
				user.setPseudo(pseudo);
			}
			if(image != null) {
				user.setProfileImage(image);
			}
			User.save(user);
			response.getWriter().write(user.toJSON());
		}catch(EntityNotFoundException e) {
			throw new ServletException(e.getMessage());
		}
	}
	
}
