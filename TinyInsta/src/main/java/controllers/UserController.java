package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "Users",
    urlPatterns = {"/users/*"}
)

public class UserController extends HttpServlet {

	//GET REQUESTS

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		//Some requests like /users/{id} and /users/{id}/posts/count will all be catch by this controller.
		//So we need to redirect the request depend of path informations
		String path = request.getPathInfo();
		if(path != null) {
			path = path.substring(1, path.length());
			String[] pathInformations = path.split("/");
			getUserRedirection(request, response, pathInformations);
		}
		else
		{
			UsersResponse.retrieveUsersList(request, response);
		}
	}

	private void getUserRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		// This redirection will evaluate the first argument after '/users/' in path informations
		// So : `case "count" : ... ; break;` will recognize the path '/users/count...'
		// If the first argument is an id, is value isn't defined in advance, so we use `default` to catch all possibilities
		// In any case, the action will be different depend of the number of argument :
		// '/users/{id}' will not call the same function that '/users/{id}/posts'.
		switch (pathInformations[0]) {
			case "count" :
				if(pathInformations.length == 1) {
		 			UsersResponse.countUsers(request, response);
		 		}
				break;
		 	default :
		 		if(pathInformations.length == 1) {
		 			UsersResponse.retrieveUser(request, response);
		 		}else {
		 			getUsersIdRedirection(request, response, pathInformations);
		 		}
		 		break;
		 }
	}

	//REDIRECTION LEVEL 2

	private void getUsersIdRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		// This redirection will evaluate the first argument after '/users/{id}' in path informations
		// Same principal than before
		switch (pathInformations[1]) {
		 	case "posts":
		 		if(pathInformations.length == 2) {
		 			PostsResponse.retrieveUserPostsList(request, response);
		 		} else {
		 			PostsResponse.countUserPosts(request, response);
		 		}
		 		break;
		 	case "followers":
		 		if(pathInformations.length == 2) {
		 			SubscriptionsResponse.retrieveUserFollowersList(request, response);
		 		} else {
		 			SubscriptionsResponse.countUserFollowers(request, response);
		 		}
		 		break;
		 	case "followees":
		 		if(pathInformations.length == 2) {
		 			SubscriptionsResponse.retrieveUserFolloweesList(request, response);
		 		} else {
		 			getUsersIdFolloweesRedirection(request, response, pathInformations);
		 		}
		 		break;
		 }
	}
	
	//REDIRECTION LEVEL 3

	private void getUsersIdFolloweesRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		// This redirection will evaluate the first argument after '/users/{id}' in path informations
		// Same principal than before
		switch (pathInformations[2]) {
		 	case "count":
		 		if(pathInformations.length == 3) {
		 			SubscriptionsResponse.countUserFollowees(request, response);
		 		}
		 		break;
		 	case "posts":
		 		if(pathInformations.length == 3) {
		 			PostsResponse.retrieveUserFolloweesPostsList(request, response);
		 		}
		 		break;
		 	default:
		 		if(pathInformations.length == 3) {
		 			SubscriptionsResponse.retrieveSubscription(request, response);
		 		}
		 		break;
		 }
	}

	//POST REQUESTS

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// Same redirection system than get requests, see upper
		String path = request.getPathInfo();
		if(path != null) {
			path = path.substring(1, path.length());
			String[] pathInformations = path.split("/");
			postUsersRedirection(request, response, pathInformations);
		} else {
			UsersResponse.createUser(request, response);
		}
	}

	private void postUsersRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		 switch (pathInformations[0]) {
		 	default :
		 		if(pathInformations.length > 1) {
		 			postUsersIdRedirection(request, response, pathInformations);
		 		}
		 		break;
		 }
	}

	//REDIRECTION LEVEL 2

	private void postUsersIdRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		 switch (pathInformations[1]) {
		 	case "posts":
		 		if(pathInformations.length == 2) {
		 			PostsResponse.createPost(request, response);
		 		}
		 		break;
		 	case "followees":
		 		if(pathInformations.length == 2) {
		 			SubscriptionsResponse.createSubscription(request, response);
		 		}
		 		break;
		 }
	}

	//PUT REQUESTS

	@Override
	public void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// Same redirection system than get requests, see upper
		String path = request.getPathInfo();
		if(path != null) {
			path = path.substring(1, path.length());
			String[] pathInformations = path.split("/");
			putUsersRedirection(request, response, pathInformations);
		}
	}

	private void putUsersRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		 switch (pathInformations[0]) {
		 	default :
		 		if(pathInformations.length == 1) {
		 			UsersResponse.modifyUser(request, response);
		 		}
		 		break;
		 }
	}

	//DELETE REQUESTS

	@Override
	public void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// Same redirection system than get requests, see upper
		String path = request.getPathInfo();
		if(path != null) {
			path = path.substring(1, path.length());
			String[] pathInformations = path.split("/");
			deleteUsersRedirection(request, response, pathInformations);
		} else {
			UsersResponse.createUser(request, response);
		}
	}

	private void deleteUsersRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		 switch (pathInformations[0]) {
		 	default :
		 		if(pathInformations.length > 1) {
		 			deleteUsersIdRedirection(request, response, pathInformations);
		 		}
		 		break;
		 }
	}

	//REDIRECTION LEVEL 2

	private void deleteUsersIdRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		 switch (pathInformations[1]) {
		 	case "followees":
		 		if(pathInformations.length > 2) {
		 			deleteUsersIdFolloweesRedirection(request, response, pathInformations);
		 		}
		 		break;
		 }
	}

	//REDIRECTION LEVEL 3

	private void deleteUsersIdFolloweesRedirection(HttpServletRequest request, HttpServletResponse response, String[] pathInformations) throws IOException, ServletException {
		 switch (pathInformations[2]) {
		 	default :
		 		if(pathInformations.length == 3) {
		 			SubscriptionsResponse.removeSubscription(request, response);
		 		}
		 		break;
		 }
	}

}
