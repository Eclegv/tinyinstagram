package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.JSONObject;
import database.Like;
import database.User;

public class LikesResponse {

	public static void retrievePostLikedUsersList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String postId = pathInformations[0];
		String requestPage = request.getParameter("page");
		String requestLimit = request.getParameter("limit");
		int limit = 0;
		int offset = 0;
		if(requestLimit != null) {
			try {
				limit = Integer.parseInt(requestLimit);
			} catch(Exception e) {
				e.printStackTrace();
				limit = 0;
			}
			if(requestPage != null && limit != 0) {
				try {
					int page = Integer.parseInt(requestPage);
					offset = (page - 1)*limit;
				} catch(Exception e) {
					e.printStackTrace();
					offset = 0;
				}
			}
		}
		List<User> users = Like.getPostLikeUsers(postId, offset, limit);
		response.getWriter().write(JSONObject.listToJSON(users));
	}

	public static void countPostLikes(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String postId = pathInformations[0];
		int likesNb = Like.countByPostId(postId);
		response.getWriter().write(JSONObject.intToJSON("likesCount", likesNb));
	}

	public static void retrieveLike(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String postId = pathInformations[0];
		String userId = pathInformations[2];
		Like like = Like.getByUserIdAndPostId(userId, postId);
		if(like != null) {
			response.getWriter().write(like.toJSON());
		}
	}
	
	public static void createLike(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String postId = pathInformations[0];
		String userId = request.getParameter("userId");
		Like like = Like.getByUserIdAndPostId(userId, postId);
		if(like == null) {			
			like = new Like(userId, postId);
			Like.save(like);
		}
		response.getWriter().write(like.toJSON());
	}
	
	public static void removeLike(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String path = request.getPathInfo();
		path = path.substring(1, path.length());
		String[] pathInformations = path.split("/");
		String postId = pathInformations[0];
		String userId = pathInformations[2];
		Like like = Like.getByUserIdAndPostId(userId, postId);
		Like.remove(like);
		response.getWriter().write(like.toJSON());
	}
	
}
