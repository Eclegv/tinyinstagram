package frontInterface;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "Account",
    urlPatterns = {"/account/*"}
)
public class AccountInterface extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	  String userID = request.getPathInfo().substring(1, request.getPathInfo().length());	  
	  request.setAttribute("userID", userID);
	  request.getRequestDispatcher("/WEB-INF/account.jsp").forward(request, response);
  }
}