package frontInterface;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "Option",
    urlPatterns = {"/option"}
)

public class OptionInterface extends HttpServlet {

	@Override
	  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		  request.getRequestDispatcher("/WEB-INF/appOption.jsp").forward(request, response);
	  }
	  
}
