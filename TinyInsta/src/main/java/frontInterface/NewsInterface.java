package frontInterface;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "AppNews",
    urlPatterns = {"/news"}
)
public class NewsInterface extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	  String name = request.getParameter("name");
	  request.setAttribute("name", name);
	  request.getRequestDispatcher("/WEB-INF/appNews.jsp").forward(request, response);
  }
}