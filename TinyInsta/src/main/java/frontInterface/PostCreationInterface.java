package frontInterface;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
    name = "PostCreate",
    urlPatterns = {"/postCreation"}
)
public class PostCreationInterface extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	  request.getRequestDispatcher("/WEB-INF/postCreation.jsp").forward(request, response);
  }
  
}