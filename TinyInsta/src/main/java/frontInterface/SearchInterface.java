package frontInterface;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import database.User;
import database.JSONObject;

@WebServlet(
    name = "SearchUser",
    urlPatterns = {"/search"}
)
public class SearchInterface extends HttpServlet {

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	  String query = request.getParameter("query");
	  request.setAttribute("query", query);
	  System.out.println(query);
	  request.getRequestDispatcher("/WEB-INF/appSearch.jsp").forward(request, response);
  }
  
}